# BCB 546X Project - Team TBD

## Documentation for the paper "A transcription factor hierarchy defines an environmental stress response network" by Song et al., 2016, Science. 

#### Slides for the project: `BCB546X_Group_TBD_slides.pdf`

## Project Overview

#### We focused our reanalysis on RD26 (AT4G27410). 

Raw Illumina sequence data were downloaded from the SRA for RD26 ChIP-seq datasets, converted to FASTQ format and aligned to the Arabidopsis TAIR10 reference genome. These data were then used to call peaks using MACS2 and produce BED files with their genomic locations. 

BED files containing ChIP-seq peak genomic locations from the RD26 ChIP-seq dataset were used to find the nearest genes from the Arabidopsis TAIR10 genome annotation using the R package ChIPPeakAnno. 

We also described and visualized processed ChIP-seq data for all 21 of the TFs in the Song et al., dataset (treated with or without ABA). Comparisons were made between the TFs for which ChIP-seq was performed by comparing their target genes. Corresponding RNA-seq datasets in response to ABA were visualized using heatmaps and with Dynamic Regulatory Events Miner (DREM). Networks of the ChIP-seq targets and their regulation by ABA was visualized using the network analysis software Mango. 

Finally, comparisons between the ChIP-seq data presented in Song et al., 2016 for RD26 were made to data from another paper published recently in Nature Communications by [Ye et al., 2017](https://www.nature.com/articles/ncomms14573) describing RNA-seq data for loss-of-function and gain-of-function plants for RD26. Ye et al., 2017 showed that RD26 interacts with another transcription factor called BES1 that is a master regulator of the Brassinosteroid signaling pathway. BES1 and RD26 bind cooperatively to the same promoter element (the G-box; CACGTG) within target genes and inhibit each other's function, allowing plants to tradeoff between growth (promoted by BES1 and Brassinsteroids) and drought response (promoted by RD26). We compared RD26 RNA-seq/ChIP-seq data and BES1 target genes and found that BES1 and RD26 target genes that are regulated by drought, Brassinosteroids and in RD26 overexpression were enriched for the G-box motif using DREME promoter motif analysis, thus confirming the findings of Ye et al., 2017. 

The sections below list our workflow and the person primarily responsible for each section. 

## Workflow

![](./presentation_materials/pipe_with_graphs_FINAL.png)

## 1. ChIP-seq data sets from raw sequencing reads: 
#### Led by Josh

### Files relevant to this portion of the project are located within **`processed_data/unix_processed_files/`**

#### The detailed description of the workflow can be found in `unix_pipeline.md`


The following Chip-seq datasets were used: 

[GEO link](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE80564)
[SRA link](https://www.ncbi.nlm.nih.gov/sra?term=SRP073709)

* **Controls**
	* GSM2130982: Col0_mockChIP_rep2; Arabidopsis thaliana; ChIP-Seq
	* GSM2130981: Col0_mockChIP_rep1; Arabidopsis thaliana; ChIP-Seq
* **RD26 Mock Treated**
	* GSM2130924: AT4G27410_RD26_EtOH_ChIP_rep1; Arabidopsis thaliana; ChIP-Seq
	* GSM2130926: AT4G27410_RD26_EtOH_ChIP_rep2; Arabidopsis thaliana; ChIP-Seq
	* GSM2130928: AT4G27410_RD26_EtOH_ChIP_rep3; Arabidopsis thaliana; ChIP-Seq
* **RD26 ABA Treated**
	* GSM2130923: AT4G27410_RD26_ABA_ChIP_rep1; Arabidopsis thaliana; ChIP-Seq
	* GSM2130925: AT4G27410_RD26_ABA_ChIP_rep2; Arabidopsis thaliana; ChIP-Seq
	* GSM2130927: AT4G27410_RD26_ABA_ChIP_rep3; Arabidopsis thaliana; ChIP-Seq

* Note - Because of time constraints, only the following datasets finished processing:
	* GSM2130924: AT4G27410_RD26_EtOH_ChIP_rep1; Arabidopsis thaliana; ChIP-Seq
	* GSM2130923: AT4G27410_RD26_ABA_ChIP_rep1; Arabidopsis thaliana; ChIP-Seq 	

## 2. Gene annotations from ChIP-seq BED files
#### Led by Paul

### This analysis can be found in the subdirectory **`Peak_Annotation/`** 

We downloaded RD26 processed bed files from [GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE80564) and TAIR10 annotation files from [TAIR10](https://www.arabidopsis.org/download/index-auto.jsp?dir=%2Fdownload_files%2FGenes%2FTAIR10_genome_release%2FTAIR10_gff3). 

Our first pre-processing steps involved  sorting the files in ascending order and then using command line tool -bedtools intersect to annotate target genes.

We then ran [ChIPpeakAnno](https://bioconductor.org/packages/release/bioc/html/ChIPpeakAnno.html) as did the paper to annotate gene targets and make comparisons with results from the paper.



 
## 3. Processed ChIP-seq dataset description, visualization and comparisons to RNA-seq data
#### Led by Trevor

### Data for this portion of the project are located within `processed_data/`

#### The full description of the workflow and figures generated: **`processed_data/R_processed_data_project/ChIP-seq_from_processed.pdf`**. 

This portion of the analysis starts from text files containing the processed ChIP-seq target genes from each of the 21 TFs in the study either with or without ABA treatment. 

#### Processed ChIP-seq datasets

TF-target text files were downloaded from [www.ABATF.net](http://neomorph.salk.edu/dev/pages/shhuang/aba_web/pages/summary_table.php)

When the data were downloaded, each TF's targets were contained within a separate file (with the TF and treatment as the name). These were combined into one file using UNIX tools as described below. 
	
* `processed_data/genes`: all target genes for each TF (raw download from above)

			#use awk to add the filename and combine all the files
			awk '{print FILENAME "\t" $0}' * > combined.txt
			
The data were then further processed and visualized using R. A detailed description of this analysis is contained within the R notebook **`ChIP-seq_from_processed.pdf`**

* `R_processed_data_project/`: ProjectTemplate layout for R code an docs related to processed ChIP-seq datasets
	* `ChIP-seq_from_processed.Rmd`: Notebook to show analysis steps in R for processed data

* Images for processed ChIP-seq data analysis are in `processed_data/R_processed_data_project/images/`. Their naming corresponds to the code chunks in the ChIP-seq_from_processed.Rmd document. 

#### Network analysis of ChIP-seq and RNA-seq with Mango

* Network compatible files were generated inside the ChIP-seq_from_processed.Rmd document. 
* `processed_data/R_processed_data_project/output/Mango_networks/`: contained files and script to run the Mango network analysis. 

## 4. Dynamic Regulatory Events Miner (DREM) analysis of RNA-seq data

#### Led by Daniel

DREM analysis was used to visualize time course RNA-seq data in response to ABA and integrate information on TF-target gene interactions. 

### Data for this portion of the project are located within `drem/`

* See `README.md` in `drem/` for the complete process and analysis
* Briefly, 
	* Supplementary Table 1 was downloaded with differentially expressed genes
	* DAP-seq data was copied from `DAP-seq`
	* Initial files were processed with awk
	* arapidopsis_agris.txt file was taken from Drem installation directory, and it was merged with the DAP-seq modified awk file to generate the combined.txt file
	* Models were run using DREM, which was initalized from the command line while in the downloaded DREM directory, and the parameters were set as listed in `README.md` in the `drem/` directory
* File locations:
	* `drem/files` contains files used for processed
		* TableS1.txt is the downloaded supplementary table
		* DAP_TF.txt was copied from `DAP-seq`
		* 'drem/files/Expression_Data
			* TableS1_trun_titles.txt: Used for processing Figure 1B models (`drem/drem_variation_settings/Figure1B`)
			* S1-Figure_2C.txt: Used for processing Figure 2C models (`drem/drem_variation_settings/Figure2C`)
	* `drem/drem_variation_settings/` contains the models generated
		* Figure 1B in Song et al.: `Figure1B`
		* Figure 2C in Song et al.: `Figure2C`
		* More information about models available at `drem/README.md`
	* `drem/documentation`: Contains information about the version of DREM used in this analysis and how to use DREM via the DREMmanual(1).pdf

##### DAP-seq data used as part of the DREM analysis

* located in `DAP-seq/`

* interactions between TFs and targets used in DREM analysis. See `README.md` in this folder.