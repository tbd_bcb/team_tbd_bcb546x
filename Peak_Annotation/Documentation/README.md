**Download bedfiles from GEO**

	GSE80564_AT4G27410_RD26_ABA_optimal_narrowPeak_p16
	
	GSE80564_AT4G27410_RD26_EtOH_optimal_narrowPeak_p16
	
**sort the two files by chromosome and then by start position**

	sort -k1,1 -k2,2n GSE80564_AT4G27410_RD26_ABA_optimal_narrowPeak_p16.bed  > RD26_ABA_sorted.bed
	
	sort -k1,1 -k2,2n GSE80564_AT4G27410_RD26_EtOH_optimal_narrowPeak_p16.bed  > RD26_EtOH_sorted.bed
 	

**Download TAIR10 gene annotation file** from [TAIR10 gene annotation files.](https://www.arabidopsis.org/download/index-auto.jsp?dir=%2Fdownload_files%2FGenes%2FTAIR10_genome_release%2FTAIR10_gff3)

	TAIR10_GFF3_genes.gff
	
	Note: The chromosomes are named differently in the bed (chr) and the gff (Chr).
	
	vim TAIR10_GFF3_genes.gff \ :%s/Chr/chr/g

	
**Intersect GFF with bed files to find all the "peaks" that fall within genes and other features**

	bedtools intersect -b TAIR10_GFF3_genes.gff.txt -a RD26_EtOH_sorted.bed -wao > RD26_EtOH_sorted.features.txt
	
	bedtools intersect -b TAIR10_GFF3_genes.gff.txt -a RD26_ABA_sorted.bed -wao > RD26_ABA_sorted.features.txt
	

                
 [Help: bedtools intersect](http://bedtools.readthedocs.io/en/latest/content/tools/intersect.html)
 
 **Process for gene features only**
 
 	awk $13 ~ /gene/ {print$0}' RD26_EtOH_sorted.features.txt > BedAnno_EtOH_genes.txt
 	
 	awk $13 ~ /gene/ {print$0}' RD26_ABA_sorted.features.txt > BedAnno_ABA_genes.txt
 
 
	 **Results are in Peak_Annotation/Bedtools_results**
	 

####
 
 **The science paper uses an R packakeg, ChIPpeakAnno**
 
 **Method**:
 Peaks in each dynamic binding categories were associated to TAIR10 annotated genes within 1000 bp from the summit of the peaks, using ChIPpeakAnno v2.12.1
 [Help: ChIPpeakAnno ](http://physiology.med.cornell.edu/faculty/elemento/lab/data/courses/2014/CSHLseq/ChIP-seq.pdf)
 
 [Documentation/paper: ChIPpeakAnno](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-11-237)
 
 Both **R Markdown** and **R script** are available in the **Peak_Annotation/Documentation subdirectory**
 
 
 **RESULTS:**
 
 We downloaded annotated target genes from the paper to compare with our annotation results from [www.ABATF.net](http://neomorph.salk.edu/dev/pages/shhuang/aba_web/pages/summary_table.php). These are available in the sundirectory **Peak____Annotation/Song____data**
 		
 	RD26_ABA_targetGenes_5column
 	RD26_mock_targetGenes_5column
 
 Annotation in R (ChiPpeakAnno) results were further processed to subset target genes from all the other gene features including among others; exons, 3' and 5' UTR's, mRNA etc.
 
 	awk $15~/gene/{print$0}' ABA_annotatedPeak.csv > TBD_ABA_genes.txt
 	
 	awk $15~/gene/{print$0}' EtOH_annotatedPeak.csv > TBD_EtOH_genes.txt.
 	
 *Note: some further file filtering was done in excel to create a list of gene ID's from each which were then imported into [Venny 2.1.0](http://bioinfogp.cnb.csic.es/tools/venny/), a venn diagrams tool for comparing up to four lists of elements.
 
 We made comparisons between our annotated genectargets and the paper's annotated gene targets and we found;
 	
 	* A total of 14161 annotated genes and pseudogenes vs Song's 15299 ABA genes representing 70% similarity.
 	
 	* For the mock experiment, we found 8938 genes vs 9429 genes from Song et al results and out of this, 64 % were similar.
 	
 	
 Lastly we did a comparison using our bedtools results and found 13571 ABA target genes and 8161 for mock treatment which is much lower than ChIPpeakAnno results. 
 
  