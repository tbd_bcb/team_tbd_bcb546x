# File Manipulation
Since Drem requires input parameters, the following commands were run:
<br>
### Obtain and Manipulate Expression Data
First, the expression data required for Drem had to be downloaded from the supplementary materials (Table S1).

This file had to be manipulated because it will otherwise throw a Null error 

* Take the first three lines and write the last line of these lines (minus the first three symbols)  to a new file
	* ```head -n 3 TableS1.txt | tail -n 1 | cut -c 3- > TableS1_trun_titles.txt```

* Take line four to the end of TableS1 and append to the TableS1_trun_titles.txt
	* ``` tail -n+4 TableS1.txt >> TableS1_trun_titles.txt```

**TableS1-trun_titles.txt is now the expression data used throughout this analysis**
<br>
### Obtain and Manipulate Transcription Factor Interaction Data (TF-gene_Interactions)
* arabidopsis_agris.txt is provided in Drem, but it was copied to drem/TF-gene_Interactions
* DAP-seq data was generated in song_et_al_2017/DAP-seq. It is manipulated here
	* ```echo -e 'TF\tGene\tInput' > DAP_TF.txt```

### Merge the Agris & DAP-TF data
In order to run trials with the DAP-seq and Agris data merged, the Agris TF data must first be manipulated. To do this, do the following:

* Copy arabidopsis_agris.txt.gz from the drem directory

	* ```cp arabidopsis_agris.txt.gz ~/song_et_al_2017/drem/files/TF-gene_Interactions/```

* Unzip the file, so it has a .txt extension

	* ```gunzip -d arabidopsis_agris.txt.gz```

* While in the TF-gene_Interactions directory, make a new file with the combined data:
 	* ```cat ./DAP_TF.txt > ./combined.txt```
 	* ```tail -n+2 arabidopsis_agris.txt >> ./combined.txt```

**combined.txt now contains both DAP_TF.txt and Agris TF-gene interaction data.**
<br>
<br>

# Drem Parameter differences:
Since parameters were not described in detail in Song et al., parameters were tested to try to find the model closest to the paper in an attempt to recreate Figures 1B and 2C.
<br>

The following models were run in Drem 2.0, which is available from http://www.sb.cs.cmu.edu/drem/. To run drem, open the command line and write: 

```
java -mx1024M -jar drem.jar
```

Then, using the GUI, set the parameters to the following for each trial.
<br>
##Agris TF-gene Interactions
Agris is built into the Drem 2.0 GUI, so there is no need to find this file from an outside source.

## Expression Data
Expression data was downloaded from Table S1, 

## Examine Settings for Figure 1B

### Trial 1:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalize data
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization, add 0
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 3:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Log normalize data
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 4:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalize data
4. Penalty: 40 (Default for DREM)
**Increasing the penalty means fewer networks**
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 5:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization/add 0
4. Penalty: 40 (Default for DREM)
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 6:
1. Using Agris as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Log normalize data
4. Penalty: 40 (Default for DREM)
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels


<br>
##DAP-seq TF-gene Interactions
### Trial 1:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalize data
4. Penalty: 40
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization, add 0
4. Penalty: 40
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 3:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Log normalized
4. Penalty: 40
5. Gene annotation source: TAIR
6. Show time series
7. Show key regulatory labels

### Trial 4:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalized
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 5:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization, add 0
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 5:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization, add 0
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 6:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Log normalized data
4. Penalty: 25
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels
<br>
##DAP-seq + Agris

**combined.txt contains both DAP_TF.txt and Agris TF-gene interaction data.**

### Trial 1: 
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Log normalized data
4. Penalty: 40
5. **Multiple hypothesis method changed from randomization (standard) to Bonferroni**. Randomization can lead to Java errors on certain machines due to Java system resources, so the method was changed.
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2: 
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. No normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 3: 
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 4: 
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: TableS1-trun_titles.txt
3. Normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

<br>
## Model selection
After examining Figure 1B and 2C, it appears that the Agris Trial 2 model is closest to the paper figure. This model showed branching at approximately the same points as the paper did.
<br>
<br>
## Examine Settings for Figure 2C

<br>

## Figure 2C
Since Figure 2C is essentially Figure 1B with less expression data, the expression data input for Drem needs to be modified.

To do this, columns with timepoints after 8 hours need to be removed from TableS1-trun_titles.txt
```
awk -v OFS='\t' '{print $1, $2, $3, $4}' TableS1_trun_titles.txt > S1-Figure_2C.txt
```
## DAP-seq + Agris
### Trial 1:
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. No normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2:
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 3:
1. Using combined.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Log normalize data
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

## DAP-seq
### Trial 1:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. No normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 3:
1. Using DAP_TF.txt as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Log normalize data
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

## Agris
### Trial 1:
1. Using Agris as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. No normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 2:
1. Using Agris as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Normalization
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels

### Trial 4:
1. Using Agris as the TF-gene interaction source.
2. Expression data: S1-Figure_2C.txt
3. Log normalize data
4. Penalty: 40
5. Randomization setting
5. Gene annotation source: TAIR
6. Hide time series
7. Hide key regulatory labels
<br>
<br>
### After examining these models and the Agris 2 figure from Figure 1B, it seems the authors may have simply screenshoted Figure 1B (my Model Agris 2 Figure 1B) and removed the ends. This may be why my models are so different--DREM runs on a hidden Markov model and removing so many datatime points would result in fewer networks.
<br>
