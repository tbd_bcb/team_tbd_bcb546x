## Data for pre-processed ChIP-seq data from Song et al,. 2017 and for RNA-seq comparisions 

* `combined.txt`: all target genes for each TF. See project level README.md for description on how this was produced.

#### Files for TF overlap analysis

* `ChIP_ann.csv`: Annotation data used to build RTN network object for overlap analysis. In this case the "SYMBOL"
 and "PROBEID" are the same, but this is a neccessary input as this is not always the case with networks. 
* `ChIP_net.csv`: Network containing each TF ("s" column) and target gene ("t" column). Weight of interaction ("wt" column) is arbitrarily set at 1. 
* `ChIP_TF_list.csv` : list of TF_treatment combinations to be used as TF input for RTN analysis.
* `All_TF_overlap.csv`: Output of RTN overlap analysis to compare the target genes for the 21 TFs in the study. 
* `TF_names.csv`: name of each TF used in the ChIP-seq studies to be matched up with family annotation. 
* `TF_names.csv`: name of each TF and corresponding TF family - added manually according to Fig. 1E and Table S2. 

#### ABA RNA-seq data

* `GSE80565_RNAseq_ABAtimeSeries_rpkm_log2FC_FDR_filterCPM2n2.txt`: processed RNA-seq data downloaded from [GEO](https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE80565&format=file&file=GSE80565%5FRNAseq%5FABAtimeSeries%5Frpkm%5Flog2FC%5FFDR%5FfilterCPM2n2%2Etxt%2Egz). 

* `Table_S1.txt`: 3,061 DE genes from ABA treatment time course. 

	*  downloaded from [here](http://science.sciencemag.org/content/sci/suppl/	2016/11/03/354.6312.aag1550.DC1/aag1550_Table_S1.txt). 

* `Kay_TFs.csv`: 2492 Arabidopsis transcription factors from [Pruneda-Paz et al., 2014, Cell Reports](http://www.sciencedirect.com/science/MiamiMultiMediaURL/1-s2.0-S2211124714005178/1-s2.0-S2211124714005178-mmc2.xlsx/280959/html/S2211124714005178/70454d0e92136e85d5f89f26e264c86b/mmc2.xlsx). 

#### Comparisons between RD26 ChIP-seq and RNA-seq data

* located in `RD26_ChIP_RNAseq_compare/`: These gene lists were downloaded from [Ye et al., 2017] (https://www.nature.com/articles/ncomms14573) Supplementary Table 1. 
* `RD26RPM.csv`: Reads per million normalized RNA-seq reads for RD26 overexpression and mutant data (provided by Yin lab). 




