## Mango Networks

* Produced using Mango version 1.24. 
* Network can be recreated using code by running the script `gel.txt` using the mango program

#### Output

* currently network visualization in Mango can only be taken via screenshot. I indicated where in the script I took these. 
	* **`ABA_TF_net_mango.png`**: Network of ChIP-seq interactions between the 21 ABA related TFs in the Song et al., 2017 Science paper with the 3061 ABA regulated genes. Red indicates ABA up regulated while blue indicates ABA down regulated. TFs are colored yellow. 
		* labels added: **`ABA_TF_net_mango_labels.png`**
	* **`RD26_subnet_mango.png`**: subnetwork from `ABA_TF_net_mango` only including connections to RD26 and ANAC102. 
		* labels added **`RD26_subnet_mango_labels.png`**
	* Labels for the nodes were added in PowerPoint `Mango_net_labels.pptx` 

	