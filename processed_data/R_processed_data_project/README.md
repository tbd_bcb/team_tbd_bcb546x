# R_processed_data_project

## This directory contains the code and files to analyzed pre-processed ChIP-seq data from Song et al,. 2017. 

* `ChIP-seq_from_processed.Rmd`: R markdown notebook for plotting and exploring the ChIP-seq and RNA-seq data. A detailed description of the files and analysis is provided here and in the corresponding PDF `ChIP-seq_from_processed.pdf`

#### Data used in the analysis and their description is provided in `data/`

#### output images are located in `images/`

* The description of these images corresponds to the R code chunk in `ChIP-seq_from_processed.Rmd`

#### Network visualization of the ChIP-seq RNA-seq `output/Mango_networks/`
* see README in this directory for further description. 

