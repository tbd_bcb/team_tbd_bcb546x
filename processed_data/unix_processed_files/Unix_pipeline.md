# Preprocessing and processing of files on the command line
This markdown serves as both documentation and a full example of how one would download data and process it using the Unix command line in order to
analyze the data for a ChIP-Seq analysis

The steps of processing files so that they are able to be used in a ChIP-Seq analysis are as follows:
 1.  Generate or download data from a database
 2.  Convert data to fastq format and perform quality check
 3.  Align fastq files to a reference genome
 4.  Call peaks from .bam or .sam files

I will go into further detail concerning how each step is performed in this document. A few important things should be noted:
- To perform this sort of data processing, one must have access to a sufficiently powerful computer. I have access to, and am using the hpc-class computing cluster
available to me through my enrollment in a class, 546x. The examples which I use in this document will therefore be dependent on access to this cluster. If you
wish to perform the same sort of data processing, you will need to have access to, and know the address of a computing cluster
- All files which are generated in this example will be available in the folder `Unix_processed_files`
- Because of the large size of the files which are used and generated in this example, and because I have access to limited space on the hpc-class machine, and 
have even more limited time, I will be processing two files, one containing a control sample, and one containing an abscisic acid treated sample, but the methods described here could easily be extended to 
larger data sets should the need arise 
- In the case of each command run on hpc-class, or any other remote machine which is shared between users and which uses a scheduler, one will have to run a script for
execution by the scheduler. I describe the process of using this script once, and then omit further mention of it, as all of the scheduler scripts that I use for this 
analysis are available in the `Unix_processed_files` directory of this bitbucket repository. It should be noted that, while I omit mention of scheduler scripts, I use
one for each command run for each step
- Initially I had intended to upload each intermediary file, bitbucket did not like this plan however, and has threatened our group with read only access should we upload too much data. Thus, in the interest of not preventing my groupmates from adding files, I have elected to not add the files which I produced during this pipeline, as I also am a poor college student who cannot afford to pay for extra storage space.
### Before processing
Iowa State uses the Slurm scheduler. Detailed information is available at `https://slurm.schedmd.com/`. I use the Slurm script generator available at 
`http://www.hpc.iastate.edu/guides/classroom-hpc-cluster/slurm-job-script-generator`. An example of a slurm script is as follows:
```
#!/bin/bash
#SBATCH --time=0:15:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=1   # number of nodes
#SBATCH --ntasks-per-node=1   # 1 processor core(s) per node 
#SBATCH --job-name="sra_download"
#SBATCH --mail-user=jtwalker@iastate.edu   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --output="sra_download_job.out" # job standard output file (%j replaced by job id)
#SBATCH --error="sra_download_job_err.out" # job standard error file (%j replaced by job id)

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
module load sratoolkit
prefetch -v SRR3418094
```
Each command that I run in the processing of files is run through the Slurm scheduler as in the example above. 

After creating a Slurm script in the form of a `.txt` file, you can run your script with `sbatch slurm_file.txt`

Before processing files, you will need to connect to a computing cluster (or, if your lab has a powerful machine, omit this step). If you are connecting from off campus,
you will need to connect to the campus network using a vpn. Directions on how to download, install, and use the vpn which Iowa State provides are available at
`https://www.it.iastate.edu/howtos/vpn`. Otherwise connect to the remote cluster using the following procedure:
1. Open a command line instance - this will differ from machine to machine and I will not describe the process here, though this usually only involves clicking on an icon
2. Use `ssh` to connect to the remote host:
`ssh jtwalker@hpc-class.its.iastate.edu`
3. Enter your password if prompted
You are now connected!

### 1. Generate or download data from a database
As the purpose of this project was to replicate a published paper's analysis, we did not generate any data, but instead obtained data from GEO and SRA. I will not detail how to 
switch between the file format which comes off of an Illumina sequencer and fastq format, as I have not performed this conversion myself. 
To download data from SRA, look up the name of sra or srr file that you want to download, and use the following:
`module load sratoolkit/version_desired` followed by `prefetch -v data_name`
I will be downloading 2 files for this excercies. Thus, I ran the following to obtain the data I desired:
```
module load sratoolkit/2.8.0
prefetch -v SRR3418094
prefetch -v SRR3418095
```
This returned two files:
`SRR3418094.sra` and `SRR3418095.sra`

Now that we've obtained the data we wish to process for downstream analysis, we can move on to the second step in the process.

### 2. Convert data to fastq format and perform quality check
We currently have `.sra` files, but we will need `.fastq` files in order to align our data to a reference genome. 
To convert to `.fastq` from `.sra`, we will again use `sratoolkit`, which is still loaded from the last command. To convert between file types we will use the `fastq-dump` tool.
The general syntax for this tool is
`fastq-dump --outdir /path_to_output_directory/ --split-files /path_to_file_for_conversion/filename.sra`
In my case, I did not care about seperating files into different directories based on filetype, as I was working with a small amount of memory, and was transferring files from
the cluster to my locale machine anyways - though this could be considered bad practice. I used the following to convert my data: 
```
fastq-dump --split-files SRR3418094.sra
fastq-dump --split-files SRR3418095.sra
```
This gave me two output files, `SRR3418094_1.fastq` and `SRR3418095_1.fastq`. From here, one would perform quality control if the data were new. I did this even though I knew that
my data was fine. To perform a quality check, I used the popular `fastQC` program. The general syntax for this program is:
```
module load fastqc/version_desired
fastqc filename.fastq
```
In my case, I ran the following:
```
module load fastqc/0.11.3
fastqc SRR3418094_1.fastq
fastqc SRR3418095_1.fastq
```
This gave me two output files for each file entered:
```
SRR33418094_1_fastqc.html
SRR3418094_1_fastqc.zip
```
and
```
SRR33418095_1_fastqc.html
SRR3418095_1_fastqc.zip
```
As I didn't know how to open the `.html` versions of these files on a cluster, I moved them from the computing cluster to my local machine by using the built-in Unix program
`scp`, or "secure copy" which allows for the secure transfer of files from one machine to another. The general usage for `scp` is
`scp hostname@host_machine_address:directory/file_to_transfer local_hostname@local_address:target_directory/`
in my case, this was the following:
```
scp jtwalker@hpc-class.its.iastate.edu:~/ncbi/public/sra/SRR3418094_1_fastqc.html /home/josh/group_project/
scp jtwalker@hpc-class.its.iastate.edu:~/ncbi/public/sra/SRR3418094_1_fastqc.zip /home/josh/group_project/
scp jtwalker@hpc-class.its.iastate.edu:~/ncbi/public/sra/SRR3418095_1_fastqc.html /home/josh/group_project/
scp jtwalker@hpc-class.its.iastate.edu:~/ncbi/public/sra/SRR3418095_1_fastqc.zip /home/josh/group_project/
```
On my local machine, I can then open the `.html` file by navigating to it using a graphical file finder, or on the command line by navigating to the directory and using `xdg-open filename`
to open the file

A good resource to understand the output of a fastQC report can be found here `http://www.bioinformatics.babraham.ac.uk/projects/fastqc/`

Now that we have converted our initial data type to `.fastq`, we can move on to the next step

### 3. Align fastq files to a reference genome
The next step in processing our data files for downstream analysis is alignment. The authors of this paper used the Bowtie aligner. I have used the most current version of Bowtie
in performing alignment of my `.fastq` files as the version of Bowtie used in the paper was not specified. 
Before alignment can be performed, you will either need to verify that the version of the genome you wish to use has been indexed by Bowtie. Bowtie has a number of premade indexes
available, but for this assignment, we needed to align our `.fastq` sequences to the same version of the Arabidopsis thaliana genome that the authors used, TAIR10. As there was no
prebuilt index, I built one using the `bowtie-build` command.
I first downloaded the TAIR10 genome in `.fasta` format from `https://www.ncbi.nlm.nih.gov/genome/4`, and then used `scp` to transfer the file to the computing cluster.
The general usage of `bowtie-build` is 
```
module load bowtie/version_desired
bowtie-build /path_to_genome/genome_name.fna /path_to_new_index/new_index_name
```

In my case, the command that I ran was
```
module load bowtie/1.1.2
bowtie-build GCF_000001735.3_TAIR10_genomic.fna tair10
```
This gave me an output of six files:
```
tair10.1.ebwt
tair10.2.ebwt
tair10.3.ebwt
tair10.4.ebwt
tair10.rev.1.ebwt
tair10.rev.2.ebwt
```
With the desired genome indexed, I then used Bowtie to align my `.fastq` files. Keeping in mind that Bowtie is already loaded from the previous step, the general usage of 
Bowtie is as follows:
`bowtie /path_to_genome_index/genome_index_name /path_to_fastq/filename.fastq /path_to_newly_aligned_file/filename.bam`
In my case, because I was working in one directory for the full processing pipeline, I used
```
bowtie tair10 SRR3418094_1.fastq SRR3418094.bam
bowtie tair10 SRR3418095_1.fastq SRR3418095.bam
```
This gave me an output of two files, `SRR3418094.bam` and `SRR3418095.bam`


With aligned `.bam` files, we can now move onto the last step in the processing pipeline before beginning downstream analysis

### 4. Call peaks from .bam or .sam files
This is the trickiest part of the beginning data processing step of ChIP-Seq analysis. In attempting to replicate the pipeline used in the Song et al. paper, I used only very basic settings, as I have no experience with this program, other than the available `man` page. While all other parts of this file are likely general enough for basic ChIP-Seq analysis, I would highly recommend conducting further research into which parameters are correct for your analysis. 
Before using MACS2, you will need to make sure that you have a control file containing background peak information which MACS2 will use to "subtract" from (i.e. remove all present peaks) from a given input file. I downloaded a control file which was provided by the authors of this paper, and, using the methods described above, obtained a `.bam` file. 
To reiterate - this is not a MACS2 guide, and one should not use this program without a good understanding of what the output will be. 
The general case for use of this program seems to be
```
module load macs2
macs2 callpeak -t target_file.bam -c control_file.bam -f BAM -g hs -n new_file_name -B -q 0.01
```
This is taken from an example available on the package author's github, available at `https://github.com/taoliu/MACS/` 
In my case, I used the following
```
module load macs2
macs2 callpeak -t SRR3418094.bam -c CONTROL.bam -f BAM -g hs -n SRR3418094 -B -q 0.01
macs2 callpeak -t SRR3418095.bam -c CONTROL.bam -f BAM -g hs -n SRR3418095 -B -q 0.01
```
This gave me outputs in the form of bed files, which are what one would use for further downstream analysis.
