# DAP target genes

* Reference: Ronan C. O’Malley, Shao-shan Carol Huang, Liang Song, Mathew G. Lewsey, Anna Bartlett, Joseph R. Nery, Mary Galli, Andrea Gallavotti, and Joseph R. Ecker (2016). Cistrome and Epicistrome Features Shape the Regulatory DNA Landscape, Cell. 2016 May 19;165(5):1280-92. doi: 10.1016/j.cell.2016.04.038.

* downloaded from: http://neomorph.salk.edu/dap_web/pages/browse_table_aj.php

* contained in `data/dap_data_v4/` 
	* `data/dap_data_v4/all.txt`: all DAP targets with native methylation
	* `data/dap_data_v4/all_col_amp.txt`: all DAP targets without methylation (colamp desig.)
		*  both do not have headers
		*  columns are: tf.at_id	target.at_id

* code to combine all DAP-seq target genes in unix:

			# for DAP-tagets
			cd dap_data_v4
			mkdir col
			mkdir col_amp
			cp -r genes/* col/
			cd col/
			find . -name "*colamp*" -print0 | xargs -0 -I {} mv {} ../col_amp/
	
			# combine files, omitting headers. 
			# need to move output out of directory or it will loop forever
			find . -name "*.txt" -print0 | xargs -0 awk 'FNR!=1' > ../all.txt
			cd ../col_amp/
			
			find . -name "*.txt" -print0 | xargs -0 awk 'FNR!=1' > ../			all_col_amp.txt
			
